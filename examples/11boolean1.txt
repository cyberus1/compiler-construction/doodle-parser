module Example:
  bool a;
  bool b;
  bool c;
  starthere:
    a = true and false;
    b = true or true;
    c = false;
    call show with (a);
    call show with (b);
    call show with (c);
  endhere;