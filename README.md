# Compiler Construction 

*Project Work: Kittinun Taweeboon & Laura Schauer* 

## Running Code 

### Installing JavaCC

Check if you have javacc installed already: 
```javacc -version```

If not, follow these instructions: [StackOverflow](https://stackoverflow.com/questions/60589049/how-to-setup-javacc-in-windows-10)

### Run a `.jj` File

1. Compile the `.jj` file with ```javacc FileName.jj```
2. Compile the generated Java files with `javac *.java`
3. Run the program with `java FileName <your input>`, for example: 

````bash
$ java Doodle
$ {}]
Exception in thread "main" TokenMgrError: Lexical error at line 1, column 2.  Encountered: "[" (91), after : ""
        at DoodleTokenManager.getNextToken(DoodleTokenManager.java:175)
        at Doodle.jj_ntk(Doodle.java:197)
        at Doodle.MatchedBraces(Doodle.java:44)
        at Doodle.Input(Doodle.java:13)
        at Doodle.main(Doodle.java:8)
````

### JJTree for AST

We also need to create a JJTree file: JJTree is a preprocessor for JavaCC (Java Compiler Compiler) that adds support for 
constructing abstract syntax trees (ASTs) during parsing. 

An explanation I found on Stackoverflow [(here)](https://stackoverflow.com/questions/13902239/how-to-implement-jjtree-on-grammar):

> Creating an AST using JavaCC looks a lot like creating a "normal" parser (as defined in the `jj` file).
> Here are the steps needed to create an AST: 
> 1. Rename your `.jj` file to `.jjt` (The `.jj` file will later get generated when invoking jjtree)
> 2. *Decorate* it with *root-labels* (the italic words are my own terminology...)
> 3. Invoke jjtree on your `jjt `grammar, which will generate a `jj` file for you 
> 4. Invoke javacc on your generated `jj` grammar 
> 5. Compile the generated java source files 
> 6. Test it

To run jjtree, simply add the following command before the usual `JavaCC` commands: 

`jjtree <jjt-filename`

So altogether, you should run: 

````bash
jjtree parser.jjt
javacc parser.jj
javac *.java
java Doodle <example-code>
````